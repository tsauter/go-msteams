package msteams

type TeamsData struct {
	Type       string  `json:"@type"`
	Context    string  `json:"@context"`
	Summary    string  `json:"summary"`
	ThemeColor string  `json:"themeColor,omitempty"`
	Sections   []Group `json:"sections,omitempty"`
}

type Group struct {
	StartGroup       bool   `json:"startGroup,omitempty"`
	Title            string `json:"title,omitempty"`
	ActivityTitle    string `json:"activityTitle,omitempty"`
	ActivitySubtitle string `json:"activitySubtitle,omitempty"`
	Facts            []Fact `json:"facts,omitempty"`
}

type Fact struct {
	Name  string `json:"name,omitempty"`
	Value string `json:"value,omitempty"`
}

func NewTeamsData(text string) (*TeamsData, error) {
	data := &TeamsData{
		Type:    "MessageCard",
		Context: "http://schema.org/extensions",
		Summary: text,
	}

	return data, nil
}

func (td *TeamsData) AddSectionGroup(g Group) error {
	td.Sections = append(td.Sections, g)
	return nil
}
