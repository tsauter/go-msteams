package msteams

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

type Webhook struct {
	URL string
}

func NewWebhook(url string) (*Webhook, error) {
	return &Webhook{URL: url}, nil
}

func (wh *Webhook) SendToWebhook(td *TeamsData) error {
	jsondata, _ := json.Marshal(td)
	//fmt.Printf("%s\n", jsondata)

	client := &http.Client{}
	req, err := http.NewRequest("POST", wh.URL, bytes.NewReader(jsondata))
	if err != nil {
		return err
	}
	req.Header.Set("Content-type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("%s", resp.Status)
	}

	return nil
}
